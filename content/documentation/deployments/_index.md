---
title: Deployment Instructions
description: Step by step instructions for deploying the project to various environments.
weight: 10
--- 

### Local Deployment

[This guide](local-deploy/) will explain and show how to deploy a testing router on your local machine. It will cover the following:
