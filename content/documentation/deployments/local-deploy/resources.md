---
title: Resources and Attributions
description: A collection of the links and articles used in the Local Deploy guide.
weight: 60
---

### Languages

- [GoLang](https://golang.org)
- [TypeScript](https://www.typescriptlang.org)  

### Frameworks / Libs / Playgrounds
 
- [Angular.io](https://angular.io) 
- [AngularCLI](https://cli.angular.io/)
- [Go Tour](https://tour.golang.org/welcome/1)
- [NodeJS](https://nodejs.org) 
- [Nexus Framework](https://github.com/gammazero/nexus/wiki)
- [Autobahn-Browser](https://github.com/crossbario/autobahn-js-browser) 
- [Crossbar.io](https://crossbar.io)

### Articles / Books / Specs

- [WAMP v2](https://wamp-proto.org)
- [PubSub](https://en.wikipedia.org/wiki/Publish–subscribe_pattern)
- [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call)
- [Monolith](https://en.wikipedia.org/wiki/Monolithic_application)
- [Design Patterns](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Bob Martin - Single Responsibility Principle](http://butunclebob.com/ArticleS.UncleBob.SrpInRuby) 
- [Coding Horror - Curly's Law](https://blog.codinghorror.com/curlys-law-do-one-thing/)