---
title: Security Manager Module
description: Orchestrates auth flows and provides encryption services.
weight: 20 
---

This module has not yet started development..

## Features

- **Auth(z)** - Controls the authentication and authorization methods needed for user roles. and account security features.

- **Session Tokens** - A token dispensary that organizes persistence for a user.

- **Access Tokens** - A token dispensary that assigns and refreshes tokens.

