---
title: Back End MicroProjects
description: Descriptions and definitions for the ambient components of the distributed system.
weight: 30
---

|Project | Repo | Board|
|:---|:----:|:---:|
| Data Store Manager | [GitHub](https://github.com/CodeForPortland/a2j-back-end_data-store-manager) | [Waffle.io](https://waffle.io/CodeForPortland/a2j-back-end_data-store-manager/join) |
| User Manager | [GitHub](https://github.com/CodeForPortland/a2j-back-end_user-manager) | [Waffle.io](https://waffle.io/CodeForPortland/a2j-back-end_user-manager/join) |
| File Services Module  | Not Setup Yet | Not Setup Yet | 
| Document Builder Module | Not Setup Yet | Not Setup Yet | 
| Security Manager | Not Setup Yet | Not Setup Yet | 
 
 
#### User Manager

An orchestrator for initiating session, login and registrations procedures and for providing responses to the front end clients.

- overview
- interface

#### File Service Module

The client for initiating document builds for providing downloadable or shareable files for a front end client. 

- overview
- interface

#### Document Builder Module

Builds the documents from profiles and templates into the various documents in the required formats.

- overview
- interface

#### Data Store Module

Manages the data base connections and provides general and secure *CRUD* tooling for non-public clients.

- overview
- interface

#### Security Manager Module

Provides authentication and authorization tokens, also organizes and initiates hashing procedures and encryption/decryption for all the things.

- [overview](security)
- interface

