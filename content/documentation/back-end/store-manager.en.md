---
title: Store Services Module
description: A basic CRUD provider and ORM that presents tooling for data store management.
weight: 30
---

The store services provide object relational mapping and data base connection clients for basic CRUD operations.


## Internal Procedures

- Model.Action(...options) - Actions can be `Create, Update, Read`. Destroying data is not allowed yet.   


## Contributors

{{% ghcontributors owner="CodeForPortland" repo="a2j-back-end_data-store-manager" %}}
