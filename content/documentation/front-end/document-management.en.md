---
title: Document Management Module
description: An Angular6 Module that provides the tooling to edit and organize documents on the file system.
weight: 50
updatedon: 2018/11/18
---

Document Management is a module that provides a WYSIWYG for editing the text on a generated document. It also allows a user to edit the meta details attached to a document and organize the document files with a mobile first file system GUI. 

## Features

- **Editor** - A UI that contains navigable views from nav-tabs to switch between editable sections of text from a generated document and viewing and editing the metadata attached to that document.

- **File Navigator** A file system navigation interface that is mobile first that allows a user the ability to group, search and edit or move files and folders.

- **Share/Print** Registers `share` and `print` with the dashboards top right button as well as on the file navigator, this feature is for providing those outside the system, documents with or without redaction in specified formats including `PDF`.

## Interface

...coming soon

## Contributors

{{% ghcontributors owner="CodeForPortland" repo="a2j-front-end_document-manager" %}}
