---
title: Dashboard Module
description: An Angular6 Module that provides the entry point for user engagement.
weight: 10
updatedon: 2018/11/18
---

The dashboard will provide the landing and end user login and registration services with help and about views. 
It also provides a way for other front end components to integrate or broadcast services.

## Features

**Login** - The login is a form interface that accepts user credential information and returns a session token to provide role based access and custom data persistence.

**Registration** - A form input that accepts a new users request with credentials to create a new account. On success a session token is given in response and the user is logged in.

**Dashboard Tiles** - A grid list of the primary features offered by registered front end components.

**Navigation Bar** - The top bar provides navigable links to a help view and custom actions provided by the current view. 
Also the logo provides a back to home link and a back button for navigating to the previous content view.   

**Content Display** - The bottom segment under the header is a view portal that displays current content within mobile constraints.

## Interface

...coming soon

## Contributors

{{% ghcontributors owner="CodeForPortland" repo="a2j-front-end_dashboard" %}}
