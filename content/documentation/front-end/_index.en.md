---
title: Front End MicroProjects
description: Documentation on the front end modules.
weight: 30
---

|Project | Repo | Board|
|:---|:----:|:---:|
| WWW | [GitHub](https://github.com/CodeForPortland/Access2JusticePDX) | [Waffle.io](https://waffle.io/CodeForPortland/access2justicePDX/join) |
| Dashboard | [GitHub](https://github.com/CodeForPortland/a2j-front-end_dashboard) | [Waffle.io](https://waffle.io/CodeForPortland/a2j-front-end_dashboard/join) |
| Document MGT Module | [GitHub](https://github.com/CodeForPortland/a2j-front-end_document-manager) | [Waffle.io](https://waffle.io/CodeForPortland/a2j-front-end_document-manager/join) |
| Interview Wizard | [GitHub](https://github.com/CodeForPortland/a2j-front-end_interview-wizard) | Not Setup Yet |
| Profile MGT Module | [GitHub](https://github.com/CodeForPortland/a2j-front-end_profiles) | [Waffle.io](https://waffle.io/CodeForPortland/a2j-front-end_profiles/join) |
| Account MGT Module | Not Setup Yet | Not Setup Yet |

### Dashboard

The landing page for the application. It will control the login and service/module registrations to allow a user to navigate to different features.

- [Overview](dashboard)
- Interface

### Interview Wizard

The interview wizard is an interface for a question and answers process. This will provide a user to provide values for the variable inputs that are needed in a generated document. A great example is provided by [DocAssemble](https://docassemble.org/demo.html).

- [Overview](interview-wizard)
- Interface

### Profiles 

- [Overview](profiles)
- Interface

### Document Management

The document manager is an interface that will allow the user to search and organize the various documents which have been generated, or they can create a new one.

- [Overview](document-management)
- Interface

### Account Management

This interface will be for account managers, to provide access rules to different users, view histories and reset credentials.

- Overview 
- Interface