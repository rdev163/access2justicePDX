---
title: Our Second Bi-Week Code Night
description: Hung out at Luck Labs to talk about our projects and code a little. 
weight: 20
---

### Discussions 

Most of the discussion was about the architecture of the project. A few questions revolved around the [local deploy guide](/documentation/deployments/) and there was some confusion about the breaking changes made with the repo links and in the cloning section. 
We also discussed about changing the location of the meeting due to the sound quality of the environment (It's loud). Other discussions where about how to keep people who were interested in the project but do not code updated. 
One of the solutions for this problem you are reading right now. :)

### Developments

Some of the front end development has begun, Aidan has been working on the [Interview Wizard](https://github.com/CodeForPortland/a2j-front-end_interview-wizard). 
Currently it has some reusable form components that cane be generated from a JSON like data object and there is a plan to look into Google's auto-complete API. 
The [Document Manager](https://github.com/CodeForPortland/a2j-front-end_document-manager) is also nearing it's ability to be called a prototype. 
It has a functional **WYSIWYG** that has a few bugs but those can be taken care of later with not too much tech debt and Rex's currently working on a document file management system to pair with some back end services for storing instances of generated documents.  

## Closing Thoughts -  A Volunteer Geist Triage 

The difficulties of having a volunteer group of workers are showing. I think there are a few reasons for this. A common one said is they don't have the time to commit to this project.
This is very opposite of what I was trying to set. I want there to be small palatable parts and I think  the officialdom of titles seems to be a bit heavy. Being a "lead" or "PM" implies responsibilities which implies a need to organize and devote time. Which is working with no pay, bleh! 
I think something more like a bounty board model would be better. I think I will be doing away with official titles, there's enough momentum without the need to have a designated project lead or manager. Less politics. I think if we build and document it well enough, then others can follow along. If they want to. So, instead of demanding time from others with labels and duty, we provide opportunities to contribute and pathways of information to them.

> If you build it, they will come.   