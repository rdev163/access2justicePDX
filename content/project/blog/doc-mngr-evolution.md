---
title: Evolution of the Document Manager
description: A trail of screenshots showing the various points of development for the front end document management module.
weight: 30
---

The document manager was a bit more complex then I had first thought it would be. It's still not complete but it is far 
enough along to allow us to start building and integrating other services, I am primarily concerned with getting the data stores 
and the document builder and encoder up. We need to map out the processes for redaction and authorizing document generation/sharing
but they can be done while we use dummy data and can start user testing so we can get a sense of what revisions would be wanted as we 
wire in the back. 

## Screen Captures - File Navigator
 
Here are a few screen shots of the evolution of the File Navigator, this component will allow a user to walk through their folders 
which would consist of other folders and instances of already generated or built documents they have access too. The module it's self
also has a reusable WYSIWYG that can be used to provide textual edits to a document. 


![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-6.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-5.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-4.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-3.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-2.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-1.png)

![A screen cap of the file navigator](/access2justicePDX/project/blog/doc-mngr-evolution/file-nav-0.png)

