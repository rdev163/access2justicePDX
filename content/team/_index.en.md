---
title: Team
weight: 20
chapter: true
---
# The Team

[CodeForPDX.org](https://codeforpdx.herokuapp.com) is a [Code for America](https://codeforamerica.org) Brigade, part of a national network of civic-minded volunteers who contribute their skills toward using the web as a platform for local government and community service. 

