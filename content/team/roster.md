---
title: Team Roster
description: Information and details on each team member for Access2Justice
---

>Access2Justice is a volunteer driven project and could not be done with out these awesome people. 

|Name|Position|Information|
|:-----|:---:|:---:|
| John E. Grant, J.D. | Owner| [The Commons Law Center](thecommonslawcenter.org)|
| Hugh | Brigade Captain | [Hugh@CodeForPDX.org](hugh@codeforpdx.org)|
| Robert Rex Beatie| Lead Engineer | [Rex@CodeForPDX.org](rex@codeforpdx.org) [RDev Rocks the Web](https://rdev.rocks) |
| Aidan Fleischer | Project Manager / Engineer | [Portfolio](https://github.com/afleischer) |
| Alex J | Engineer | N/A |
| Esti | Engineer | N/A |
| Claire | Engineer | N/A |
| Alex A | Engineer | N/A |
